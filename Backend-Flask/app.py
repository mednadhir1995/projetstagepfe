from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
import uuid
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
import datetime
from functools import wraps
from werkzeug.utils import secure_filename
from ocr import  Process
import os
import requests
import json
from elasticsearch import Elasticsearch
from flask_restful import Resource, Api, reqparse
from flask_cors import CORS
app = Flask(__name__)
cors = CORS(app, resources={r"/ocr/*": {"origins": "*"}})

UPLOAD_FOLDER = './UploadDir'
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
ALLOWED_EXTENSIONS = set([  'png', 'jpg', 'jpeg', 'gif'])


app.config['SECRET_KEY'] = 'thisissecret'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:123456789@localhost/users'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False




db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(50), unique=True)
    username = db.Column(db.String(80), unique=True)
    password = db.Column(db.String(120), unique=True)
    loginSF = db.Column(db.String(120), unique=True)
    passwordSF = db.Column(db.String(120), unique=True)
    client_id = db.Column(db.String(120), unique=True)
    client_secret = db.Column(db.String(120), unique=True)
    security_token = db.Column(db.String(120), unique=True)


    def __init__(self,public_id, username, password,loginSF,passwordSF,client_id,client_secret,security_token):
        self.public_id=public_id
        self.username = username
        self.password = password
        self.loginSF = loginSF
        self.passwordSF = passwordSF
        self.client_id = client_id
        self.client_secret = client_secret
        self.security_token = security_token

def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if not token:
            return jsonify({'message' : 'Token is missing!'}), 401

        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
            current_user = User.query.filter_by(public_id=data['public_id']).first()
        except:
            return jsonify({'message' : 'Token is invalid!'}), 401

        return f(current_user, *args, **kwargs)

    return decorated

@app.route('/ocr/register', methods=['POST'])
def register():
    status = 'this user is already registered'
    json_data = request.json
    try:
        if not request.json['username'] or not request.json['password'] or not request.json['loginSF'] or not \
        request.json['passwordSF'] or not request.json['client_id'] or not request.json['client_secret'] or not \
        request.json['security_token']:
            status = 'Please enter all the fields'
        else:
            us = User.query.filter_by(username=json_data['username']).first()
            if us is None:
                hashed_password = generate_password_hash(request.json['password'], method='sha256')
                new_user = User(public_id=str(uuid.uuid4()), username=json_data['username'],
                password=hashed_password,
                loginSF=json_data['loginSF'],
                passwordSF=request.json['passwordSF'],
                client_id=json_data['client_id'],
                client_secret=json_data['client_secret'],
                security_token=json_data['security_token'])
                db.session.add(new_user)
                db.session.commit()
                status = 'User registered successfully'

    except:
        status = 'this user is already registered'
    db.session.close()
    return jsonify({'result': status})


@app.route('/ocr/login',methods=['POST'])
def login():
    json_data = request.json
    print(json_data['username'])
    print(json_data['password'])
    if not json_data or not request.json['username'] or not request.json['password']:
        return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})

    user = User.query.filter_by(username=json_data['username']).first()

    if not user:
        return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})

    if check_password_hash(user.password, json_data['password']):
        token = jwt.encode({'public_id' : user.public_id,'username':user.username, 'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=60)}, app.config['SECRET_KEY'])

        return jsonify({'token' : token.decode('UTF-8')})

    return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})



def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS




#function that extract fields for the email field and website field) from list
def extractMatchers(l:list):
    for line in l:
        if (":" in line):
            return line.split(':')[1]
        else:
            return line


#ocr function that digitilize the image into text
# 'multipart/form-data  =>Postman  form data file : img.jpg
@app.route('/ocr/Upload', methods=['POST'])
@token_required
def upload_file(current_user):
    if not current_user is None:
        if request.method == 'POST':
            # check if the post request has the file part
            if 'file' not in request.files:
                return jsonify({"error": "No file part"})
            file = request.files['file']
            if file.filename == '':
                return jsonify({"error": "No file selected for uploading"})
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                print(filename)
                resultOCR,rep= Process(filename)
                if rep==200:
                    list_str = resultOCR.split('\r\n')
                    while ("" in list_str):
                        list_str.remove("")
                        # list_str.remove(str(email))
                        # list_str.remove(str(website))

                    print(list_str)
                    result = [x for x in list_str if not any(c.isdigit() for c in x)]
                    result2 = [x for x in list_str if any(c.isdigit() for c in x)]

                    print(result)
                    print(result2)
                    matchersE = ['@', 'Å', 'Email', 'EMAIL', 'email', 'E-mail']
                    emailL = [s for s in result if any(xs in s for xs in matchersE)]
                    matchersW = ['www.', 'Website', 'WEBSITE', 'website','www?','www']
                    webL = [s for s in result if any(xs in s for xs in matchersW)]

                    email = extractMatchers(emailL)
                    website = extractMatchers(webL)
                    bad_chars = ['-rel:', 'Tel', 'TéL', 'GSM', 'Gsm', 'tel', 'Fax', 'FAx', 'Address:', ':']
                    res = []
                    for string in result2:
                        for i in bad_chars:
                            string = string.replace(i, "")
                        res.append(string)
                    print('res::', res)
                    while email in result:
                        result.remove(email)

                    while website in result:
                        result.remove(website)
                    if website is None:
                        website = '###########'
                    if email is None:
                        email = '###########'

                    return jsonify({"Success": "File successfully uploaded",
                                    "listesCh": result,
                                    "listesNb": res,
                                    "email": email,
                                    "website": website})
                else:
                    return jsonify({"error": "Allowed file types are  png, jpg, jpeg"})
            else:
                return jsonify({"error": "OCR Server down"})

#first node document for the firstnames fields
NODE_NAME = 'names'
es = Elasticsearch([{'host': 'localhost', 'port': 9200}])
parser = reqparse.RequestParser()
parser.add_argument("name", type=str, required=True, help="name parameter is Required ")
#search class for the firstname fields using elasticsearch query
class Search(Resource):
    def __init__(self):
        self.name = parser.parse_args().get("name", None)
        print(self.name)
        self.baseQuery = {
            "query":{
                "wildcard":{
                        "firstname":str(self.name)+"*"
                                }
                            }
                        }


    def get(self):
        # print("In")
        # value = str(self.rating)
        # self.baseQuery["query"]["wildcard"]["firstname"]= "{}*".format(value)
        res = es.search(index=NODE_NAME,  body=self.baseQuery)
        print(res)

        return jsonify(res)
api = Api(app)
#first api that use elasticsearch for the firstname field
api.add_resource(Search, '/ocr/elastic/name')

#second node document for the lastnames fields
NODE_NAME2 = 'lastnames'
parser2 = reqparse.RequestParser()
parser2.add_argument("lastname", type=str, required=True, help="lastname parameter is Required ")

#search class for the lastname fields using elasticsearch query
class SearchLastname(Resource):
    def __init__(self):
        self.lastname = parser2.parse_args().get("lastname", None)
        print(self.lastname)
        self.baseQuery = {
                          "query":{
                                    "wildcard":{
                                          "lastname":str(self.lastname)+"*"
                                          }
                         }
        }



    def get(self):
        res = es.search(index=NODE_NAME2,  body=self.baseQuery)
        print(res)

        return jsonify(res)
api2 = Api(app)
#second api that use elasticsearch for the laststname field
api2.add_resource(SearchLastname, '/ocr/elastic/lastname')

#second node document for the lastnames fields
NODE_NAME3 = 'companynames'
parser3 = reqparse.RequestParser()

parser3.add_argument("company", type=str, required=True, help="CompanyName parameter is Required ")

#search class for the lastname fields using elasticsearch query
class SearchCompanyName(Resource):
    def __init__(self):
        self.company = parser3.parse_args().get("company", None)
        print(self.company)
        self.baseQuery = {
                          "query":{
                                    "wildcard":{
                                          "company":str(self.company)+"*"
                                          }
                         }
        }



    def get(self):
        res = es.search(index=NODE_NAME3,  body=self.baseQuery)
        print(res)

        return jsonify(res)


api3 = Api(app)
#third api that use elasticsearch for the company name  field
api3.add_resource(SearchCompanyName, '/ocr/elastic/company')



url_ins2='/services/data/v25.0/sobjects/Lead/'
err='erreur authentification'

def getToken(login,password,client_id,client_secret,security_sf):
    url_api = "https://login.salesforce.com/services/oauth2/token?grant_type=password"
    password =password+security_sf
    params={"username":login,
            "password":password,
            "client_id":client_id,
            "client_secret":client_secret}
    result = requests.post(url_api,params=params)
    rep=result.status_code
    if(rep==200):
        result = result.content.decode()
        result = json.loads(result)
        return rep,result
    else:
        return rep,err




def senddata(instance,token,body):
    url_ins=instance+ url_ins2
    print(url_ins)
    print("send",token)

    result = requests.post(url_ins ,data= body,  headers={'Authorization': 'Bearer '+token ,
                  'Accept': 'application/json, text/plain, */*',            "Content-Type":"application/json"})
    print(result.status_code)
    if result.status_code ==201:
        return "record saved into salesforce "
    else:
        return "saving error "



#this is webservice that handle saving data into salesforce
@app.route('/ocr/add', methods = ['POST'])
@token_required
def postJsonHandler(current_user):
    print (request.is_json)
    content = request.get_json()
    body = content['lead']
    id=current_user.public_id
    body = json.dumps(body)

    if not id is None:
        user = User.query.filter_by(public_id=id).first()

        rep,res = getToken(user.loginSF,user.passwordSF,user.client_id,user.client_secret,user.security_token)

        if(rep==200):
            print(res)
            s1 = json.dumps(res)
            y = json.loads(s1)
            token = y.get("access_token")
            print(token)
            instance = y.get("instance_url")
            print(instance)
            data = senddata(instance, token,body)
            # data = getdata(instance,token)
            return jsonify({
                "response": 'ok',
                "result" : data

            })
        else:
            return jsonify({"response": res})
    else:
        return jsonify({"response": "you must be authentified"}),401










if __name__ == '__main__':
    app.run()
