import requests
import io
import json
import os
import cv2
import re
def startOcr(url):
    response = requests.get(url)
    image_bytes = io.BytesIO(response.content)

    # Ocr
    url_api = "https://api.ocr.space/parse/image"
    # _, compressedimage = cv2.imencode(".jpg", roi, [1, 90])
    # file_bytes = io.BytesIO(compressedimage)

    result = requests.post(url_api,
                  files = {"screenshot.jpg": image_bytes},
                  data = {"apikey": "helloworld",
                          "language": "eng"})

    result = result.content.decode()
    result = json.loads(result)

    parsed_results = result.get("ParsedResults")[0]
    text_detected = parsed_results.get("ParsedText")
    return text_detected

def Process(filename):
    PATH =os.path.join("./UploadDir/",filename)
    print(PATH)
    img = cv2.imread(PATH)
    height, width, _ = img.shape
    img = cv2.resize(img, (1239,702))
    # Ocr
    url_api = "https://api.ocr.space/parse/image"
    _, compressedimage = cv2.imencode(".jpg", img, [1, 90])
    file_bytes = io.BytesIO(compressedimage)

    result = requests.post(url_api,
                  files = {"screenshot.jpg": file_bytes},
                  data = {"apikey": "helloworld",
                          "language": "eng",
                          "detectOrientation":"True"
                          })
    rep=result.status_code
    result = result.content.decode()
    result = json.loads(result)

    parsed_results = result.get("ParsedResults")[0]
    text_detected = parsed_results.get("ParsedText")
    print(text_detected)
    return text_detected,rep

def Extract(str):
    tel = re.search(pattern=" [+(0-9) -0-9]+", string=str).group()
    print("phone",tel)
    email=re.search(pattern=r"[a-zA-Z0-9!#&$!?_{}-~]+([a-zA-Z0-9!#&$!?_{}-~]+)*@([a-z0-9]+([a-z0-9-]*))+[.a-zA-Z]+",string=str).group()
    print("email:",email)
    website=re.search(pattern=r"([www]+\.[a-zA-Z0-9]+\.[a-zA-Z]+)", string=str).group()
    print("website:",website)
    # new_data=str
    # str=str.splitlines()
    # print(str)
    return str,tel,email,website
#def Clean(list):
    # list.remove(a)
    # list.remove(b)
    # list.remove(c)
    # [x for x in list if x]
    #return list

