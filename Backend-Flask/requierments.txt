aniso8601 @ file:///home/conda/feedstock_root/build_artifacts/aniso8601_1613958697960/work
brotlipy==0.7.0
certifi==2021.5.30
cffi @ file:///C:/ci/cffi_1600681203656/work
chardet==3.0.4
click @ file:///home/linux1/recipes/ci/click_1610990599742/work
cryptography @ file:///C:/ci/cryptography_1601046909128/work
cycler==0.10.0
dlib @ https://pypi.python.org/packages/da/06/bd3e241c4eb0a662914b3b4875fc52dd176a9db0d4a2c915ac2ad8800e9e/dlib-19.7.0-cp36-cp36m-win_amd64.whl#md5=b7330a5b2d46420343fbed5df69e6a3f
elasticsearch @ file:///home/conda/feedstock_root/build_artifacts/elasticsearch_1616526198655/work
Flask @ file:///home/ktietz/src/ci/flask_1611932660458/work
Flask-Cors @ file:///tmp/build/80754af9/flask-cors_1598886879133/work
Flask-RESTful==0.3.8
Flask-SQLAlchemy @ file:///home/conda/feedstock_root/build_artifacts/flask-sqlalchemy_1616102249273/work
html5lib @ file:///tmp/build/80754af9/html5lib_1593446221756/work
idna @ file:///tmp/build/80754af9/idna_1593446292537/work
itsdangerous==1.1.0
Jinja2 @ file:///home/linux1/recipes/ci/jinja2_1610990516718/work
joblib==1.0.0
kiwisolver==1.3.1
lxml @ file:///C:/ci/lxml_1603216357477/work
MarkupSafe==1.1.1
matplotlib==3.3.3
mkl-fft==1.3.0
mkl-random==1.1.1
mkl-service==2.3.0
mysql-connector-python==8.0.18
nameparser==1.0.5
olefile @ file:///home/conda/feedstock_root/build_artifacts/olefile_1602866521163/work
opencv-contrib-python==4.4.0.46
pandas @ file:///C:/ci/pandas_1602088210462/work
Pillow @ file:///D:/bld/pillow_1614689858460/work
protobuf==3.6.0
pycparser @ file:///tmp/build/80754af9/pycparser_1594388511720/work
PyJWT==1.7.1
PyMySQL @ file:///C:/ci/pymysql_1601582421099/work
pyOpenSSL @ file:///tmp/build/80754af9/pyopenssl_1594392929924/work
pyparsing==2.4.7
PySocks==1.7.1
pyspellchecker==0.5.2
pytesseract @ file:///home/conda/feedstock_root/build_artifacts/pytesseract_1605866581960/work
python-dateutil==2.8.1
pytz @ file:///home/conda/feedstock_root/build_artifacts/pytz_1612179539967/work
regex @ file:///C:/ci/regex_1602770568056/work
requests @ file:///tmp/build/80754af9/requests_1592841827918/work
six==1.15.0
soupsieve==2.0.1
SQLAlchemy @ file:///C:/ci/sqlalchemy_1598356696886/work
textblob==0.15.3
threadpoolctl==2.1.0
tqdm @ file:///tmp/build/80754af9/tqdm_1602185206534/work
urllib3 @ file:///tmp/build/80754af9/urllib3_1603305693037/work
webencodings==0.5.1
Werkzeug @ file:///home/ktietz/src/ci/werkzeug_1611932622770/work
win-inet-pton==1.1.0
wincertstore==0.2
