# Title / Flask Webservices

## About

Using Flask to build a Restful API Server that Digitize Business Cards and save into a salesforce organisation .

## Before You Begin
Before you begin we recommend you read about the basic building blocks that assemble this application:

* Flask - The best way to understand express is through its [Official Website](https://flask.palletsprojects.com/en/2.0.x/), which has a  guide, as well as an  guide for general flask topics.s a great starting point.

* ElasticSearch - Start by going through [Node.js Official Website](https://www.elastic.co/fr/elastic-stack), which should get you going with the ELK platform in no time.

## Prerequisites
Make sure you have installed all of the following prerequisites on your development machine:
* Git - [Download & Install Git](https://git-scm.com/downloads). OSX and Linux machines typically have this already installed.

* Python - [Download & Install Python 3.6.12](https://www.python.org/downloads//)

* Elasticsearch - [Download & Install Elasticsearch 7.12.0 ](https://www.elastic.co/fr/elasticsearch/)

* Postman - [Download & Install Elasticsearch 7.12.0 ](https://www.postman.com/downloads/)

* Mysql Workbench - [Download & Install Mysql Workbench](https://dev.mysql.com/downloads/installer/), and make sure it's running on the default port .

* Anaconda (Optional)- [Download & Install Anaconda](https://docs.anaconda.com/anaconda/install/windows/),you can use anaconda and create with it an envirment to execute the backend or you can simply use your local machine (it's your choice).

## Configuration
to use elaticsearch along in this project you need to add some code into elaticsearch.yml to allow elasticsearch connect with browsers (CORS Configuration)

cd  C:\ProgramData\Elastic\Elasticsearch\config

add this code in the bottom of file:
```
http.cors.allow-origin: "*"
http.cors.enabled: true
http.cors.allow-credentials: true
http.cors.allow-methods : OPTIONS, HEAD, GET, POST, PUT, DELETE
http.cors.allow-headers: X-Requested-With, X-Auth-Token, Content-Type, Content-Length, Authorization, Origin, Access-Control-Allow-Headers, Accept
```
## Creating INDEXes  in elasticsearch
1 - open postman, make sure that elasticsearch running in background
2 - put this url (http://localhost:9200/names/_doc/) and put the method =POST
3 - PUT IN THE body -> raw ->json
 {
   "name":"jhon"
}

4 - send , in the  response you will see that index(names) created:true

we will nedd two other indexes for our application :


5 - put this url (http://localhost:9200/lastnames/_doc/) and put the method =POST
6 - PUT IN THE body -> raw ->json
 {
   "lastname":"brown"
}

7 - send , in the  response you will see that index(lastnames) created:true

8 - put this url (http://localhost:9200/companyNames/_doc/) and put the method =POST
9 - PUT IN THE body -> raw ->json
 {
   "company":"amazon"
}

10 - send , in the  response you will see that index(companyNames) created:true
that's it.


## Installation
A little intro about the installation.
```
$ git clone https://NadhirX4@bitbucket.org/NadhirX4/flask-webservices.git


```
first you need either to use anaconda environment  or work in your local environment
open anaconda prompt then :

to use anaconda environment (https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-with-commands)
```
$ cd ../path/to/the/file

```
then enter your env and Install libraries  with pip in local machine or inside a conda env:

```
$ pip install -r requirements.txt

```
## Creating Database
you will execute this file only one time to create a connection with sql workbech and create a table user
```
import mysql.connector
cnx = mysql.connector.connect(user='root', password='yourpassword',
                              host='localhost')



my_cursor=cnx.cursor()

my_cursor.execute('CREATE DATABASE users')
my_cursor.execute('SHOW DATABASES')
for db in my_cursor:
    print(db)


```

## Run Flask

```
$ python app.py

```
In flask, Default port is 5000



### components
OCR.space API
Elasticsearch Cluster
Salesforce Endpoint








