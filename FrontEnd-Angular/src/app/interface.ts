export interface Response {
    _shards: {};
    hits: Hits;
    timed_out: boolean;
    took: number;
  }

export interface Hits {
    hits: Items;
    max_score: number;
  }

export declare type Items = Item[];

export interface Item {
    _id: string;
    _index: string;
    _score: number;
    _source: {
        firstname: string
    };
  }
