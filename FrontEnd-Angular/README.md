# Project

Using angular  to build a web application  that Digitize Business Cards and save into a salesforce organisation .This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.1.



## Installation
***
A little intro about the installation.
```
$ git clone  https://NadhirX4@bitbucket.org/NadhirX4/angular-ocr-frontend.git
$ cd ../path/to/the/file
$ npm install
$ npm start
```
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.



## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
